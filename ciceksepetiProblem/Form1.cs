﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Device.Location;

namespace ciceksepetiProblem
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        
        DataTable dt = new DataTable();
        GeoCoordinate seller_red = new GeoCoordinate(41.049792, 29.003031);
        GeoCoordinate seller_green = new GeoCoordinate(41.069940, 29.019250);
        GeoCoordinate seller_blue = new GeoCoordinate(41.049997, 29.026108);
        int quota_red = 0, quota_green = 0, quota_blue = 0;
        DataTable order_data = new DataTable();
        List<int> distribution_blue = new List<int>();
        List<int> distribution_green = new List<int>();
        List<int> distribution_red = new List<int>();

        private void Form1_Load(object sender, EventArgs e)
        {
            OleDbConnection connection = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Users\sametemirog\source\repos\ciceksepetiProblem\ciceksepetiProblem\data\siparisler.xlsx; Extended Properties='Excel 12.0;'");
            connection.Open();
            OleDbCommand data = new OleDbCommand("SELECT * FROM [Siparişler$]",connection); 
            OleDbDataAdapter da = new OleDbDataAdapter(data);
            da.Fill(dt);
            dataGridView1.DataSource = dt.DefaultView;
            connection.Close();
            String x1, x2, y1, y2;
            x1 = dt.Rows[0][1].ToString();
            x2 = dt.Rows[1][1].ToString();
            y1 = dt.Rows[0][2].ToString();
            y2 = dt.Rows[1][2].ToString();
            x1 = x1.Replace(".",",");
            x2 = x2.Replace(".", ",");
            y1 = y1.Replace(".", ",");
            y2 = y2.Replace(".", ",");

            Double latitude1, longitude1, latitude2, longitude2;
            latitude1 = Convert.ToDouble(x1);
            latitude2 = Convert.ToDouble(x2);
            longitude1 = Convert.ToDouble(y1);
            longitude2 = Convert.ToDouble(y2); 

            GeoCoordinate seller = new GeoCoordinate(latitude1, longitude1);
            GeoCoordinate order = new GeoCoordinate(latitude2, longitude2);
            double distance = seller.GetDistanceTo(order);
            label1.Text = distance.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
           
            for (int i = 0; i < order_data.Rows.Count; i++)
            {
                if (order_data.Rows[i][1].ToString() == "b")
                {
                    if (distribution_blue.Count < 20)
                    {
                        distribution_blue.Add(Convert.ToInt32(order_data.Rows[i][0]));
                        quota_blue++;
                    }
                }
                else if (order_data.Rows[i][1].ToString() == "g")
                {
                    if (distribution_green.Count < 35)
                    {
                        distribution_green.Add(Convert.ToInt32(order_data.Rows[i][0]));
                        quota_green++;
                    }
                }
                else if (order_data.Rows[i][1].ToString() == "r")
                {
                    if (distribution_red.Count < 20)
                    {
                        distribution_red.Add(Convert.ToInt32(order_data.Rows[i][0]));
                        quota_red++;
                    }
                }
            }

            for (int b = 0; b < distribution_blue.Count; b++)
            {
                moveAndDelete(distribution_blue[b].ToString());
            }

            for (int g = 0; g < distribution_green.Count; g++)
            {
                moveAndDelete(distribution_green[g].ToString());
            }

            for (int r = 0; r < distribution_red.Count; r++)
            {
                moveAndDelete(distribution_red[r].ToString());
            }
            
            dataGridView1.DataSource = order_data;
            label1.Text = quota_blue.ToString() + " " + quota_green.ToString() + " " + quota_red.ToString();
        }

        public void moveAndDelete(String order)
        {
            for (int i = 0; i < order_data.Rows.Count; i++)
            {
                if (order_data.Rows[i][0].ToString() == order)
                {
                    order_data.Rows[i].Delete();
                }
            }
        }
        
        public static string Compare(double r, double g, double b)
        {
            if (r < g)
            {
                if (r < b) return "r";
                else return "b";
            }
            else
            {
                if (g < b) return "g";
                else return "b";
            }
        }

        public static double MeasurementQuota(double x1, double x2, String seller)
        {
            if (seller == "b")
            {

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String o_X, o_Y;
            double d_r, d_g, d_b;
            int order_number;
            order_data.Columns.Add("Sipariş Numarası");
            order_data.Columns.Add("Renk Kodu");
            order_data.Columns.Add("Mesafe");

            for (int i = 0; i < 100; i++)
            {
                order_number = Convert.ToInt32(dt.Rows[i][0]);
                o_X = dt.Rows[i][1].ToString();
                o_Y = dt.Rows[i][2].ToString();
                o_X = o_X.Replace(".", ",");
                o_Y = o_Y.Replace(".", ",");
                double latitude = Convert.ToDouble(o_X);
                double longitude = Convert.ToDouble(o_Y);
                GeoCoordinate order = new GeoCoordinate(latitude,longitude);
                d_r = seller_red.GetDistanceTo(order);
                d_g = seller_green.GetDistanceTo(order);
                d_b = seller_blue.GetDistanceTo(order);

                DataRow rows = order_data.NewRow();

                String seller_color = Compare(d_r,d_g,d_b);
                if (seller_color == "r")
                {
                    rows["Sipariş Numarası"] = order_number;
                    rows["Renk Kodu"] = seller_color;
                    rows["Mesafe"] = d_r;
                }
                else if (seller_color == "g")
                {
                    rows["Sipariş Numarası"] = order_number;
                    rows["Renk Kodu"] = seller_color;
                    rows["Mesafe"] = d_g;
                }
                else if (seller_color == "b")
                {
                    rows["Sipariş Numarası"] = order_number;
                    rows["Renk Kodu"] = seller_color;
                    rows["Mesafe"] = d_b;
                }
                order_data.Rows.Add(rows);
                dataGridView1.DataSource = order_data;
                order_data.DefaultView.Sort = "Renk Kodu ASC,Mesafe ASC";
            }
        }
    }
}